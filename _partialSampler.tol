//////////////////////////////////////////////////////////////////////////////
// FILE   : _partialSampler.tol
// PURPOSE: Declares class BysMcmc::PartialSampler and related ones
// PURPOSE: Generic handler for partial simulation of MCMC
// Partial simulation allows to fix some parameters of a MCMC or reload some 
// others from a previously stored MCMC in order to draw faster
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
//Base Class to inherite MCMC reloading engines
Class @McmcReloader
//////////////////////////////////////////////////////////////////////////////
{
  Real _.sequential;
  Real _.burnin = ?;
  Real _.sampleLength = ?;
  Real _sequenceKey = ?;
  
  ////////////////////////////////////////////////////////////////////////////
  Set selectIndexes(Set selectedNames)
  ////////////////////////////////////////////////////////////////////////////
  {
    Copy(Empty)
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix reloadSimulation(Real numSim, Set indexes)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Rand(0,1,0,0) 
  };

  ////////////////////////////////////////////////////////////////////////////
  Real initialize.sequence(Real unused)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real _sequenceKey := If(_.sequential, 0, IntRand(1,99999999));
    1
  };

  ////////////////////////////////////////////////////////////////////////////
  Real getRowSequence(Real numSim)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(_.sequential, 
    {
      _.burnin+1+numSim%_.sampleLength
    },
    {
      Real sequence = DeterministicPseudoRandomInteger(numSim+_sequenceKey);
      _.burnin+1+sequence%_.sampleLength
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @McmcReloader Null(Real unused)
  ////////////////////////////////////////////////////////////////////////////
  {
    BysMcmc::@McmcReloader aux =
    [[
      Real _.sequential = True
    ]]
  }


};


//////////////////////////////////////////////////////////////////////////////
//Stores all fixed and reloaded parameters for full MCMC and the method of
//reloading old simulations
Class @McmcPartialHandler
//////////////////////////////////////////////////////////////////////////////
{
  //Names of all fixed parameters in the MCMC
  Set _.fixedParam = Copy(Empty);
  //Stores values of fixed parameters in natural order
  VMatrix _.fixedValues = Rand(0,1,0,0);

  //Names of all reloaded parameters in the MCMC
  Set _.reloadParam  = Copy(Empty);
  //Reloader of old simulations
  BysMcmc::@McmcReloader _.reloader;

  //Null @McmcPartialHandler does nothing
  Static @McmcPartialHandler Null(Real unused)
  {
    BysMcmc::@McmcPartialHandler aux = [[
      BysMcmc::@McmcReloader _.reloader = BysMcmc::@McmcReloader::Null(0) 
    ]]
  };

  //Builds a generic instance of @McmcPartialHandler
  Static @McmcPartialHandler New(
    Set fixedParam,        //Names of fixed parameters       
    VMatrix fixedValues,   //Values of fixed parameters
    Set reloadParam,       //Names of reloaded parameters       
    @McmcReloader reloader  //Reloading handler
  )
  {
    BysMcmc::@McmcPartialHandler aux = [[
      Set _.fixedParam  = SetTolNameAndIndex(fixedParam);    
      VMatrix _.fixedValues = fixedValues;
      Set _.reloadParam = SetTolNameAndIndex(reloadParam);    
      BysMcmc::@McmcReloader _.reloader = reloader ]];
    aux
  }

};


//////////////////////////////////////////////////////////////////////////////
//Stores information about partial simulation related to a specific block 
//where this object will be a member
Class @BlockPartialSampler
//////////////////////////////////////////////////////////////////////////////
{
  //Full MCMC partial sampling handler
  BysMcmc::@McmcPartialHandler _.mcmcPartialHandler = 
    BysMcmc::@McmcPartialHandler::Null(0);

  //Total number of parameters in the block
  Real _.N = ?; 
  //Names of fixed parameters of the block
  Set _.fixedParam = Copy(Empty);
  //Indexes of fixed parameters inside the block
  Matrix _.fixedParamIdx  = Rand(0,1,0,0);
  Real _.fixedParamNum = ?;

  //Names of reloaded parameters of te block
  Set _.reloadParam  = Copy(Empty);
  //Indexes of reloaded parameters inside the block
  Matrix _.reloadParamIdx  = Rand(0,1,0,0);
  Real _.reloadParamNum = ?;

  //Indexes of reloaded parameters inside the original MCMC to reload
  Set _.reloadParamIdxOrig  = Copy(Empty);

  //Names of all block parameters but fixed and reloaded
  Set _.freeParam = Copy(Empty);
  //Indexes of all block parameters but fixed and reloaded
  Matrix _.freeParamIdx = Rand(0,1,0,0);
  Real _.freeParamNum = ?;

  //Stores values of fixed parameters in natural order
  VMatrix _.fixedValues = Rand(0,1,0,0);
  //Stores values of reloaded parameters in natural order
  VMatrix _.reloadValues = Rand(0,1,0,0);
  //Stores values of both fixed and reloaded parameters in the block order
  VMatrix _.values = Rand(0,1,0,0);
  VMatrix _.nonFreeValues = Rand(0,1,0,0);

  Set _.nonFreeIndex = Copy(Empty);
  Set _.freeIndex = Copy(Empty);


  ////////////////////////////////////////////////////////////////////////////
  Static @BlockPartialSampler Null(Real unused)
  ////////////////////////////////////////////////////////////////////////////
  {
    BysMcmc::@BlockPartialSampler aux = [[ Real _.N = 0 ]];
    aux
  };

  ////////////////////////////////////////////////////////////////////////////
  //Selects fixed and reloaded parameters related to a block
  Real build(Set blockParam, 
             @McmcPartialHandler mcmcPartialHandler)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE @BlockPartialSampler::build 1");
    Real _.N := Card(blockParam);
    BysMcmc::@McmcPartialHandler _.mcmcPartialHandler := mcmcPartialHandler;
    Set fixedParamIdx = Copy(Empty);
    Set reloadParamIdx = Copy(Empty);
    Real   _.fixedParamNum  := 0;
    Real   _.reloadParamNum := 0;
    Real   _.freeParamNum   := 0;
    Matrix _.fixedParamIdx  := Rand(0,1,0,0);
    Matrix _.reloadParamIdx := Rand(0,1,0,0);
    Matrix _.freeParamIdx   := Rand(0,1,0,0);
    Set    _.fixedParam     := Copy(Empty);
    Set    _.reloadParam    := Copy(Empty);
    Set    _.freeParam      := Copy(Empty);
    Set    _.reloadParamIdxOrig := Copy(Empty);
    If(!_.N,
    {
      1
    },
    {
      Real If(Card(_.mcmcPartialHandler::_.fixedParam), 
      {
        Set   fixedParamAll  = _.mcmcPartialHandler::_.fixedParam;
        Set   fixedParamAux  = SelectIndexByName(blockParam, fixedParamAll);
        VMatrix _.fixedValues := 
          SubRow(_.mcmcPartialHandler::_.fixedValues, fixedParamAux);
        Real If(Card(fixedParamAux), 
        {
          Set _.fixedParam := ExtractByIndex(fixedParamAll, fixedParamAux);
          Set EvalSet(_.fixedParam, Text(Text prm){PutName(ToName(prm),prm)});
          Real If(Card(_.fixedParam), SetIndexByName(_.fixedParam));
          Set fixedParamIdx := SelectIndexByName(_.fixedParam, blockParam);
          Matrix _.fixedParamIdx := SetCol(fixedParamIdx);
          1 
        })
      });
      Real If(Card(_.mcmcPartialHandler::_.reloadParam), 
      {
        Set   reloadParamAll  = _.mcmcPartialHandler::_.reloadParam;
        Set   reloadParamAux  = SelectIndexByName(blockParam, reloadParamAll);
        Real If(Card(reloadParamAux), 
        {
          Set _.reloadParam    := ExtractByIndex(reloadParamAll, reloadParamAux);
          Set EvalSet(_.reloadParam, Text(Text prm){PutName(ToName(prm),prm)});
          Real If(Card(_.reloadParam), SetIndexByName(_.reloadParam));
          Set reloadParamIdx := SelectIndexByName(_.reloadParam, blockParam);
          Matrix _.reloadParamIdx := SetCol(reloadParamIdx);
          Set _.reloadParamIdxOrig := 
           _.mcmcPartialHandler::_.reloader::selectIndexes(_.reloadParam);
          1 
        })
      });
      Set freeParamIdx = Range(1,_.N,1)-(fixedParamIdx << reloadParamIdx);
      Matrix _.freeParamIdx := SetCol(freeParamIdx);
      Set _.freeParam := ExtractByIndex(blockParam, freeParamIdx);
      Real _.fixedParamNum  := Rows(_.fixedParamIdx);
      Real _.reloadParamNum := Rows(_.reloadParamIdx);
      Real _.freeParamNum   := Rows(_.freeParamIdx);
      Real If(_.fixedParamNum | _.reloadParamNum,
      {
        getMergedFixedAndReloadedSample(0)
      });
      1
    });
    Set _.nonFreeIndex := MatSet(Tra(_.fixedParamIdx << _.reloadParamIdx))[1];
    Set _.freeIndex := MatSet(Tra(_.freeParamIdx))[1];
    1
  };

  ////////////////////////////////////////////////////////////////////////////
  Set getNonFreeIndex(Real unused)
  ////////////////////////////////////////////////////////////////////////////
  {
    _.nonFreeIndex
  };

  ////////////////////////////////////////////////////////////////////////////
  Set getFreeIndex(Real unused)
  ////////////////////////////////////////////////////////////////////////////
  {
    _.freeIndex
  };


  ////////////////////////////////////////////////////////////////////////////
  Real getMergedFixedAndReloadedSample(Real numSim)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE"+_MID+" @BlockPartialSampler::getMergedFixedAndReloadedSample 0");
    VMatrix _.reloadValues := _.mcmcPartialHandler::_.reloader::
     reloadSimulation(numSim, _.reloadParamIdxOrig);
  //WriteLn("TRACE"+_MID+" _.fixedValues:"<<_.fixedValues);
  //WriteLn("TRACE"+_MID+" _.reloadValues:"<<_.reloadValues);
    VMatrix values = Case(
    !_.fixedParamNum, 
    {
      MergeRows(_.N, [[
        [[_.reloadValues, _.reloadParamIdx]] ]])
    },
    !_.reloadParamNum, 
    {
      MergeRows(_.N, [[
        [[_.fixedValues,  _.fixedParamIdx ]] ]])
    },
    1==1,
    {
      MergeRows(_.N, [[
        [[_.fixedValues,  _.fixedParamIdx ]],
        [[_.reloadValues, _.reloadParamIdx]] ]])
    });
    VMatrix _.values := Convert(values,"Blas.R.Dense");
    VMatrix _.nonFreeValues := SubRow(_.values, _.nonFreeIndex);
    0
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix mergeWithFreeSample(VMatrix freeValues)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE"+_MID+" @BlockPartialSampler::mergeWithFreeSample 0");
  //WriteLn("TRACE"+_MID+" _.fixedValues:"<<_.fixedValues);
  //WriteLn("TRACE"+_MID+" _.fixedParamIdx:"<<_.fixedParamIdx);
  //WriteLn("TRACE"+_MID+" _.reloadValues:"<<_.reloadValues);
  //WriteLn("TRACE"+_MID+" _.reloadParamIdx:"<<_.reloadParamIdx);
  //WriteLn("TRACE"+_MID+" freeValues:"<<freeValues);
  //WriteLn("TRACE"+_MID+" _.freeParamIdx:"<<_.freeParamIdx);
    VMatrix values = MergeRows(_.N, [[
      [[_.fixedValues,  _.fixedParamIdx ]],
      [[_.reloadValues, _.reloadParamIdx]],
      [[  freeValues,   _.freeParamIdx  ]] ]]);
    VMatrix _.values := Convert(values,"Blas.R.Dense")
  }

};


