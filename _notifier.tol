//////////////////////////////////////////////////////////////////////////////
// FILE   : _notifier.tol
// PURPOSE: Declares Class BysMcmc::@Notifier
// PURPOSE: Event tracing handler
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Class @Notifier
//////////////////////////////////////////////////////////////////////////////
{
  Real parsing(Real unused) { Real 0 };
  Real initializing(Real numVar,Text path) { Real 0 };
  Real simulating(Real numSim, Real timeAvg) { Real 0 };
  Real reporting(Real unused) { Real 0 };
  Real evaluating(Real unused) { Real 0 };
  Real saving(Real unused) { Real 0 };
  Real ending(Real unused) { Real 0 };

  //////////////////////////////////////////////////////////////////////////////
  Static @Notifier Null(Real unused)
  //////////////////////////////////////////////////////////////////////////////
  {
    BysMcmc::@Notifier aux =
    [[
      Real _void = ?
    ]]
  }

};
  

////////////////////////////////////////////////////////////////////////////
Class @Notifier.DB : @Notifier 
////////////////////////////////////////////////////////////////////////////
{
  Text _.dbName;
  Text _.model;
  Text _.session;
  Real _.nErr = Copy(NError);
  Real _.nWar = Copy(NWarning);
  NameBlock _table = BysMcmc::Bsr::DynHlm::DBApi::_.table;

  //Generic events notifier
  Real notify_messages(Real unused)
  {
    Real DBExecQuery(
    "UPDATE "+_.dbName+"."+_table::EstimStatus+"\n"+
    "SET nu_error = "+IntText(NError-_.nErr)+" \n"+
    "WHERE id_model='"+_.model+"' AND id_session='"+_.session+"';");
    Real DBExecQuery(
    "UPDATE "+_.dbName+"."+_table::EstimStatus+"\n"+
    "SET nu_warning = "+IntText(NWarning-_.nWar)+" \n"+
    "WHERE id_model='"+_.model+"' AND id_session='"+_.session+"';");
    1
  };
  //Generic events notifier
  Real notify_process(Text processField)
  {
    WriteLn("[dbNotifier] notify_process("+processField+")");
    Real notify_messages(0);
    Real DBExecQuery(
    "UPDATE "+_.dbName+"."+_table::EstimStatus+"\n"+
    "SET "+processField+" = "+
    BysMcmc::Bsr::DynHlm::DBApi::SqlFormatDate(Now)+" \n"+
    "WHERE id_model='"+_.model+"' AND id_session='"+_.session+"';");
    1
  };

  //Model load event notifier
  Real loading(Real unused)
  {
    Real notify_process("dh_loading")
  };

  //Model parser event notifier
  Real parsing(Real unused)
  {
    Real notify_process("dh_parsing")
  };

  //Initialization event notifier
  Real initializing(Real numVar, Text path)
  {
    Real notify_process("dh_initializing");
    Real DBExecQuery(
    "UPDATE "+_.dbName+"."+_table::EstimStatus+"\n"+
    "SET nu_mcmc_var = "+IntText(numVar)+" \n"+
    "WHERE id_model='"+_.model+"' AND id_session='"+_.session+"';");
    Real DBExecQuery(
    "UPDATE "+_.dbName+"."+_table::EstimStatus+"\n"+
    "SET te_path_data_out = '"+path+"' \n"+
    "WHERE id_model='"+_.model+"' AND id_session='"+_.session+"';");
    1
  };

  //Simulation events notifier
  Real simulating(Real numSim, Real timeAvg)
  {
    Real If(!numSim, notify_process("dh_simulating"), notify_messages(0));
    Real DBExecQuery(
    "UPDATE "+_.dbName+"."+_table::EstimStatus+"\n"+
    "SET nu_mcmc_currentLength = "+IntText(numSim)+" \n"+
    "WHERE id_model='"+_.model+"' AND id_session='"+_.session+"';");
    Real DBExecQuery(
    "UPDATE "+_.dbName+"."+_table::EstimStatus+"\n"+
    "SET vl_mcmc_time_avg = "<<timeAvg+" \n"+
    "WHERE id_model='"+_.model+"' AND id_session='"+_.session+"';");
    1
  };

  //Report event notifier
  Real reporting(Real unused) 
  { 
    Real notify_process("dh_reporting") 
  };

  //Evaluation event notifier
  Real evaluating(Real unused) 
  {
    Real notify_process("dh_evaluating")
  };

  //Evaluation event notifier
  Real saving(Real unused) 
  {
    Real notify_process("dh_saving")
  };

  //Process termination event notifier
  Real ending(Real unused) 
  {
    Real notify_process("dh_ending");
    BysMcmc::Bsr::DynHlm::DBApi::ModSes.SetBlocked(
      _.dbName, _.model, _.session, false);
    1
  };

  //////////////////////////////////////////////////////////////////////////
  Static @Notifier New(Text dbName, Text model, Text session)
  //////////////////////////////////////////////////////////////////////////
  {
    //Creates the data base notifier of current status
    BysMcmc::@Notifier.DB dbNotifier =
    [[
      Text _.dbName = dbName;
      Text _.model  = model;
      Text _.session  = session
    ]]
  }


};

