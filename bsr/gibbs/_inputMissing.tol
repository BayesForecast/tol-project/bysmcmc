/////////////////////////////////////////////////////////////////////////////
// LIBRARY: StdtLib (Standard Library of TOL)
// MODULE: Stat (Statistics)
// FILE: _inputMissing.tol
// PURPOSE: Declares method BysMcmc::Bsr::Gibbs::InputMissing
/////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
Class @InputMissingBase
////////////////////////////////////////////////////////////////////////////
{
  //--------------------------------------------------------------------------
  // Mandatory members
  //--------------------------------------------------------------------------
  //data length (number of rows of matrices in main linear regression )
  Real _.M;
  //Total number of distinct missing (input and output)
  Real _.N;

  //--------------------------------------------------------------------------
  // Auxiliar members
  //--------------------------------------------------------------------------
  //Component of dependent term of regression 
  VMatrix _.G = Rand(0,0,0,0);
  //Fixed or reloaded component to filter independent term of regression
  VMatrix _.U1 = Rand(0,0,0,0);

  //--------------------------------------------------------------------------
  //Methods
  //--------------------------------------------------------------------------

  ////////////////////////////////////////////////////////////////////////////
  Static @InputMissingBase Default(Real M, Real N)
  //////////////////////////////////////////////////////////////////////////////
  {
    BysMcmc::Bsr::Gibbs::@InputMissingBase inp = [[
    Real _.M = M;
    Real _.N = N
  ]]};

  ////////////////////////////////////////////////////////////////////////////
  Real initialize(Set i1, Set i2)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix _.G  := Zeros(_.M, Card(i2));
    VMatrix _.U1 := Zeros(_.M, 1);
    1
  };

  ////////////////////////////////////////////////////////////////////////////
  Real build_full(
    BysMcmc::Bsr::Gibbs::@BlockArima arm.blk, //ArimaBlock filter
    VMatrix si,    //SigmaBlock filter
    VMatrix beta)  //Main Linear Block parameters 
  ////////////////////////////////////////////////////////////////////////////
  {
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real build_partial(
    VMatrix store, //Current missing values  
    BysMcmc::Bsr::Gibbs::@BlockArima arm.blk, //ArimaBlock filter
    VMatrix si,    //SigmaBlock filter
    VMatrix beta)  //Main Linear Block parameters 
  ////////////////////////////////////////////////////////////////////////////
  {
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix filter(VMatrix Z, VMatrix store)
  ////////////////////////////////////////////////////////////////////////////
  {
    Z
  }

};

////////////////////////////////////////////////////////////////////////////
Class @InputMissing : @InputMissingBase
////////////////////////////////////////////////////////////////////////////
{
  //--------------------------------------------------------------------------
  // Mandatory members
  //--------------------------------------------------------------------------
  //Set of @Bsr.Missing.Info 
  Set _.inputMissingBlock;
  //Indexes of missing in full missing block
  Set _.i;

  //--------------------------------------------------------------------------
  // Auxiliar members
  //--------------------------------------------------------------------------
  //Number of non free missing in full missing block
  Real _.N1 = 0;
  //Number of free missing in full missing block
  Real _.N2 = 0;
  //Number of non free input missing in full missing block
  Real _.n1 = 0;
  //Number of free input missing in full missing block
  Real _.n2 = 0;
  //Indexes of non free input missing in full missing block
  Set _.i1 = Copy(Empty);
  //Indexes of free input missing in full missing block
  Set _.i2 = Copy(Empty);
  //Indexes of input missing in full missing block in matrix form
  Matrix _.I = Rand(0,0,0,0);
  //Indexes of non free input missing in full missing block in matrix form
  Matrix _.I1 = Rand(0,0,0,0);
  //Indexes of free input missing in full missing block in matrix form
  Matrix _.I2 = Rand(0,0,0,0);
  //Indexes of free input missing in full missing block
  Set _.i2All = Copy(Empty);
  //Indexes of columns of non free input missing
  Set _.j1 = Copy(Empty);
  //Indexes of columns of free input missing
  Set _.j2 = Copy(Empty);
  //Index of store position of each missing
  Set _.storeIndex = Copy(Empty);
  //Input missing pairs (equation,variable)
  Matrix _.tj = Rand(0,0,0,0);
  //Input missing pairs (equation,missing)
  Matrix _.tk = Rand(0,0,0,0);
  //index of columns of input missing
  Set _.j = Copy(Empty);

  VMatrix _.B  = Rand(0,0,0,0);
  VMatrix B1 = Rand(0,0,0,0);
  VMatrix B2 = Rand(0,0,0,0);
  VMatrix G2t = Rand(0,0,0,0);
  VMatrix G2 = Rand(0,0,0,0);

  //--------------------------------------------------------------------------
  //Methods
  //--------------------------------------------------------------------------

  ////////////////////////////////////////////////////////////////////////////
  Static @InputMissing New(
    Set inputMissingBlock,
    Real M, 
    Real N, 
    Set i)
  //////////////////////////////////////////////////////////////////////////////
  {
    BysMcmc::Bsr::Gibbs::@InputMissing inp = [[
    Set _.inputMissingBlock = inputMissingBlock;
    Real _.M = M;
    Real _.N = N;
    Set _.i = i
  ]]};

  ////////////////////////////////////////////////////////////////////////////
  Real initialize(Set i1, Set i2)
  ////////////////////////////////////////////////////////////////////////////
  {
    Matrix _.I := SetCol(_.i);
    Real _.N1 := Card(i1);
    Real _.N2 := Card(i2);
    Set _.i1 := i1 * _.i;
    Set _.i2 := i2 * _.i;
    Matrix _.I1 := SetCol(_.i1);
    Matrix _.I2 := SetCol(_.i2);
    Set _.i2All := i2;
    Real _.n1 := Card(_.i1);
    Real _.n2 := Card(_.i2);
    Set _.j1 := ExtractByIndex(_.j,  _.i1);
    Set _.j2 := ExtractByIndex(_.j,  _.i2);
    Set _.storeIndex := Traspose(Extract(_.inputMissingBlock, 2))[1]; 
    //Input missing pairs (equation,variable)
    Matrix _.tj := {
    //WriteLn("TRACE "+_MID+" 1");
      SetMat(Extract(_.inputMissingBlock, 3,4))
    };
    //Input missing pairs (equation,missing)
    Matrix _.tk := 
    {
    //WriteLn("TRACE "+_MID+" 2");
      SetMat(Extract(_.inputMissingBlock, 3,2))
    };
    //Input missing index j
    Set _.j := Traspose(Extract(_.inputMissingBlock, 4))[1];
    1
  };

  ////////////////////////////////////////////////////////////////////////////
  Real build_common(
    BysMcmc::Bsr::Gibbs::@BlockArima arm.blk, //ArimaBlock filter
    VMatrix si,    //SigmaBlock filter
    VMatrix beta)  //Main Linear Block parameters 
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix b_= SubRow(beta, _.j);
    Matrix  b = VMat2Mat(b_);
    VMatrix _.B := Convert(Triplet(_.tk | b, _.M, _.N), "Cholmod.R.Sparse");
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real build_full(
    BysMcmc::Bsr::Gibbs::@BlockArima arm.blk, //ArimaBlock filter
    VMatrix si,    //SigmaBlock filter
    VMatrix beta)  //Main Linear Block parameters 
  ////////////////////////////////////////////////////////////////////////////
  {
    Real build_common(arm.blk,si,beta);
    VMatrix _.G := arm.blk::filter(si*_.B);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real build_partial(
    VMatrix store, //Current missing values  
    BysMcmc::Bsr::Gibbs::@BlockArima arm.blk, //ArimaBlock filter
    VMatrix si,    //SigmaBlock filter
    VMatrix beta)  //Main Linear Block parameters 
  ////////////////////////////////////////////////////////////////////////////
  {
    Real build_common(arm.blk,si,beta);
    VMatrix _.U1 := If(!_.n1, Zeros(_.M, 1),
    {
      VMatrix B1 := SubCol(_.B,_.i1);
      VMatrix u1 = SubRow(store, _.i1);
      B1 * u1
    });
    VMatrix _.G := If(!_.n2, Zeros(_.M, _.N2),
    {
      VMatrix B2 := SubCol(_.B,_.i2);
      VMatrix G2t := Tra(arm.blk::filter(si*B2));
      VMatrix G2 := Tra(Convert(MergeRows(_.N, [[ [[ G2t, _.I2 ]] ]]),
                             "Cholmod.R.Sparse"));
      SubCol(G2, _.i2All)
    });
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix filter(VMatrix Z, VMatrix store)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix u = SubRow(store, _.storeIndex);
    Matrix tr = _.tj | VMat2Mat(u);
    VMatrix U = Triplet(tr,VRows(Z),VColumns(Z));
    Z+Convert(U,"Cholmod.R.Sparse")
  }

};

