//////////////////////////////////////////////////////////////////////////////
Class @Filter.Probit
//////////////////////////////////////////////////////////////////////////////
{
  Real      _.enabled;
  Text      _.cleanSegmentName;
  Anything  _.origOutput;
  Date      _.firstDate;
  Date      _.lastDate;

  Real _.isNonLinearFilter = True;
  Set  _.externalInfo = Copy(Empty);

  Text _.segmentName = "";
  //Identifier of original input
  Text _.inputName = "";
  //Identifier prefix for non linear parameters
  Text _.name = "";
  Text _MID = "";

  Real _.hasVectorData = ?;
  Real _.m = ?;
  Real _.numEval = ?;
  Set _.residuals_cache = Copy(Empty);

  Matrix _one = Constant(0,0,?);
  Matrix _zero = Constant(0,0,?);
  Matrix _inf = Constant(0,0,?);
  Matrix _emptyMatrix = Constant(0,0,?);
  Matrix _.Y = Constant(0,0,?);
  Matrix _.A = Constant(0,0,?);
  Matrix _.B = Constant(0,0,?);
  Matrix _residuals_average = Constant(0,0,?);
  Set _.logDensity = Copy(Empty);

  ///////////////////////////////////////////////////////////////////////////
  Real check(Real void)
  ///////////////////////////////////////////////////////////////////////////
  {
    Real outputIsBoolean = MatQuery::IsBoolean(Mat2VMat(_.Y));
    If(!outputIsBoolean, Error("Output of a probit model must have just "
      "0 or 1 values"));
    outputIsBoolean
  };

  ///////////////////////////////////////////////////////////////////////////
  Real reset(Real void)
  ///////////////////////////////////////////////////////////////////////////
  {
    Text _.segmentName := _.cleanSegmentName+"::Noise";
    //Identifier of original input
    Text _.inputName := "ProbitFilter";
    //Identifier prefix for non linear parameters
    Text _.name := "NonLinearFilterBlk::"+_.cleanSegmentName+"::ProbitFilter";
    Text _MID := "["+_.name+"]";

    Matrix _emptyMatrix := Constant(0,0,?);
    Real _.hasVectorData := Grammar(_.origOutput)=="Matrix";
    Matrix _.Y := If(_.hasVectorData, _.origOutput,
    {
      Tra(SerSetMat([[_.origOutput]],_.firstDate,_.lastDate))
    });
    Real _.m := Rows(_.Y);
    Matrix _one := Rand(1,1,1,1);
    Matrix _zero := Constant(_.m,1,0);
    Matrix _inf := _zero+1/0;
    Matrix _.A := IfMat(_.Y,_.Y*(   0),Not(_.Y)*(-1/0));
    Matrix _.B := IfMat(_.Y,_.Y*(+1/0),Not(_.Y)*(   0));

    Real _.numEval := 0;
    Set _.residuals_cache := Copy(Empty);
    Matrix _residuals_average := Constant(0,_.m,?);
    check(void)
  };

  ///////////////////////////////////////////////////////////////////////////
  Real initialize.Filter.Probit(Real unused)
  ///////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@Filter.Probit::initialize] 1");
    Real If(Card(_.externalInfo)==2,
    {
      Set Append(_.externalInfo, [[
        (_.externalInfo[1])::_.filter, 
        (_.externalInfo[2])::_.segment->EquIdx ]]);
      Real SetIndexByName(_.externalInfo)
    });
    True
  };

  ///////////////////////////////////////////////////////////////////////////
  Real initialize(Real unused)
  ///////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@Filter.Probit::initialize] 1");
    initialize.Filter.Probit(unused)
  };

/////////////////////////////////////////////////////////////////////////////
//Mandatory methods for all non linear filters
/////////////////////////////////////////////////////////////////////////////

  //Identifies the filter
  Text get.name(Real unused) { _.name };
  //Identifies the segment
  Text get.segmentName(Real unused) { _.segmentName };
  //Parameters of non linear block
  Set  get.colNames(Real unused) { Copy(Empty) };

  ///////////////////////////////////////////////////////////////////////////
  Matrix get.parameter(Real unused)
  ///////////////////////////////////////////////////////////////////////////
  {
    _emptyMatrix
  };


//Matrix _.normTrunc.lower = Constant(0,0,?);
//Matrix _.normTrunc.upper = Constant(0,0,?);

  ///////////////////////////////////////////////////////////////////////////
  Matrix eval.Filter.Probit(Matrix unused)
  //Returns the filter matrix
  ///////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE"+_MID+"eval _.externalInfo[3]=\n"<<Matrix VMat2Mat(_.externalInfo[3],1));
  //WriteLn("TRACE"+_MID+"eval _.externalInfo[4]=\n"<<Matrix SetRow(_.externalInfo[4]));
    Matrix Xb = VMat2Mat(SubRow(_.externalInfo[3],_.externalInfo[4]));
  //WriteLn("TRACE"+_MID+"eval Xb=\n"<<Matrix Tra(Xb));
    Matrix z_ = If(_.numEval==0,
    {
      WriteLn("TRACE"+_MID+"eval 2 _.numEval="<<_.numEval);
      Real corrector = Sqrt(Pi/2);
      IfMat(_.Y,_zero+corrector,_zero-corrector)
    },
    {
      Matrix z = RandTruncatedMultNormal(Xb, _one, _.A, _.B, 1, 1, False);
      Set selUnk = MatQuery::SelectRowsWithValue(Mat2VMat(Not(IsFinite(z))),1);
      If(Card(selUnk),
      {
        Real k = selUnk[1]; 
        Error("[@Filter.Probit::eval.Filter.Probit] There are "<<Card(selUnk)+
        " unknown or infinite values like : \n"+
        "  RandTruncatedMultNormal(0,1,"<<
        MatDat(_.normTrunc.lower,k,1)+","<<
        MatDat(_.normTrunc.upper,k,1)+") = "<<
        MatDat(z,k,1))
      });
      z
    });
    Real _.numEval := _.numEval+1;
  //WriteLn("TRACE"+_MID+"eval z=\n"<<Matrix Tra(z));
    _.Y-z_
  };

  ///////////////////////////////////////////////////////////////////////////
  Matrix eval(Matrix unused)
  //Returns the filter matrix
  ///////////////////////////////////////////////////////////////////////////
  {
    eval.Filter.Probit(unused)
  }

};


//////////////////////////////////////////////////////////////////////////////
Class @Filter.Probit.PriorBetaTruncated : 
  @Filter.Probit
  BysPrior::@BernouilliConjBetaTruncated
//////////////////////////////////////////////////////////////////////////////
{
  Matrix _.ij = Constant(0,0,?);
  Matrix _.nonPriorIdx = Constant(0,0,?);
  Matrix prior.y = Constant(0,0,?);
  Matrix prior.Y = Constant(0,0,?);

  ///////////////////////////////////////////////////////////////////////////
  Real check(Real void)
  ///////////////////////////////////////////////////////////////////////////
  {
    VMatrix vY = Mat2VMat(_.Y);
    Real outputIsBooleanOrUnk = MatQuery::IsBooleanOrUnk(vY);
	 
    If(!outputIsBooleanOrUnk, Warning("Output of a censored probit model "
      "must have just 0, 1 or unknown values. Values distinct of 0 or 1 "
	   "will be treated as unknown"));
	   
	  Matrix _.Y := IfMat(Or(EQ(_.Y,0),EQ(_.Y,1)),_.Y,?);
    outputIsBooleanOrUnk
  };

  ///////////////////////////////////////////////////////////////////////////
  Real setIndexes(Real void)
  ///////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@Filter.Probit.PriorBetaTruncated::setIndexes] 1");
    Matrix _.ij := _.k|Constant(_.K,1,1);
    Matrix _.nonPriorIdx:=VMat2Mat(Not(Triplet(_.ij|Constant(_.K,1,1),_.m,1)));
    Copy(_.K)
  };


  ///////////////////////////////////////////////////////////////////////////
  Real draw.output(Real void)
  ///////////////////////////////////////////////////////////////////////////
  {
    Matrix prior.y := draw(?);
  //WriteLn("TRACE [@Filter.Probit.PriorBetaTruncated::draw.output] 1 HasUnknown(prior.y)="<<MatSum(IsUnknown(prior.y)));
    Matrix prior.Y := VMat2Mat(Triplet(_.ij|prior.y,_.m,1));
    Matrix _.Y := IfMat(_.nonPriorIdx,_.Y, prior.Y);
    Matrix _.A := IfMat(_.Y,_.Y*(   0),Not(_.Y)*(-1/0));
    Matrix _.B := IfMat(_.Y,_.Y*(+1/0),Not(_.Y)*(   0));
  //WriteLn("TRACE [@Filter.Probit.PriorBetaTruncated::draw.output] 2 HasUnknown(_.Y)="<<MatSum(IsUnknown(_.Y)));
    Real PutVMatBlock(
      (_.externalInfo[1])::_.Y.orig,_.externalInfo[4][1],1,_.Y);
  //WriteLn("TRACE [@Filter.Probit.PriorBetaTruncated::draw.output] 3 HasUnknown(_.Y.orig)="<<VMatSum(IsUnknown((_.externalInfo[1])::_.Y.orig)));
    True
  };

  ///////////////////////////////////////////////////////////////////////////
  Real initialize(Real unused)
  ///////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@Filter.Probit.PriorBetaTruncated::initialize] 1");
    initialize.Filter.Probit(?);
  //WriteLn("TRACE [@Filter.Probit.PriorBetaTruncated::initialize] 2");
    Real draw.output(?);
    Real PutVMatBlock((_.externalInfo[1])::_.Y,_.externalInfo[4][1],1,_.Y);
  //WriteLn("TRACE [@Filter.Probit.PriorBetaTruncated::initialize] 3 HasUnknown(_.Y.orig)="<<VMatSum(IsUnknown((_.externalInfo[1])::_.Y)));
    True
  };

  ///////////////////////////////////////////////////////////////////////////
  Matrix eval(Matrix unused)
  //Returns the filter matrix
  ///////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@Filter.Probit.PriorBetaTruncated::eval] 1");
    Real draw.output(?);
  //WriteLn("TRACE [@Filter.Probit.PriorBetaTruncated::eval] 2");
    Matrix ev = eval.Filter.Probit(unused);
/*
    Real (_.externalInfo[2])::_.lastDrawnLogDensity := 
      (_.externalInfo[2])::_.lastDrawnLogDensity + _.lastDrawnLogDensity;
*/
    ev
  }

};

//////////////////////////////////////////////////////////////////////////////
Text _.autodoc.member.ProbitFilter =
"Builds a NameBlock that can be used as non linear filter by Class "
"@BlockNonLin in order to simulate cut coefficent of probit model "
"\n"
"  Y = F(c) + X*beta + e; e ~ N(0,s^2)        \n"
"\n"
"where\n"
"\n"
"where the non linear filter of output is defined as\n"
"\n"
"          /                                  \n"
"         | 0 - TN(0, 1, -1/0, c) if Y=0      \n"
" F(c) ~ <                                    \n"
"         | 1 - TN(0, 1, c, +1/0) if Y=1      \n"
"          \\                                 \n"
"\n"
"being TN a drawer function of an scalar random variable distributed as an "
"standard truncated normal with paraemters\n"
"\n"
"  TN(Real average, Real sigma, Real lowerBound, Real upperBound)\n"
"\n"
"Linear parameter omega will be simulated by BSR linear block.\n"
"Arguments are:\n"
"  Real      enabled     : enables/disables non linear filter \n"
"  Text      segmentName : identifier of affected output segment \n"
"  Anything  origOutput  : Series or vector of binary output \n"
"  Date      firstDate   : UnknownDate for vector data \n" 
"  Date      lastDate    : UnknownDate for vector data \n" 
"";
//////////////////////////////////////////////////////////////////////////////
NameBlock ProbitFilter
(
  Real      enabled,
  Text      segmentName,
  Anything  origOutput,
  Date      firstDate,
  Date      lastDate
)
//////////////////////////////////////////////////////////////////////////////
{
  @Filter.Probit aux = 
  [[
    Real      _.enabled = enabled;
    Text      _.cleanSegmentName = segmentName;
    Anything  _.origOutput = origOutput;
    Date      _.firstDate = firstDate;
    Date      _.lastDate = lastDate
  ]];
  Real aux::reset(?);
  aux
};


//////////////////////////////////////////////////////////////////////////////
NameBlock ProbitFilter.PriorBetaTruncated.Structured
(
  Real      enabled,
  Text      segmentName,
  Anything  origOutput,
  Date      firstDate,
  Date      lastDate,
  Set       prior 
)
//////////////////////////////////////////////////////////////////////////////
{
  @Filter.Probit.PriorBetaTruncated aux = 
  [[
    Real      _.enabled = enabled;
    Text      _.cleanSegmentName = segmentName;
    Anything  _.origOutput = origOutput;
    Date      _.firstDate = firstDate;
    Date      _.lastDate = lastDate
  ]];
  Real aux::reset(?);
  Real aux::setStructuredPrior(prior);
  Real aux::setIndexes(?);
  aux
};

//////////////////////////////////////////////////////////////////////////////
NameBlock ProbitFilter.PriorBetaTruncated.Vectorized
(
  Real      enabled,
  Text      segmentName,
  Anything  origOutput,
  Date      firstDate,
  Date      lastDate,
  Matrix    k, 
  Matrix    a, 
  Matrix    b, 
  Matrix    lower, 
  Matrix    upper
)
//////////////////////////////////////////////////////////////////////////////
{
  @Filter.Probit.PriorBetaTruncated aux = 
  [[
    Real      _.enabled = enabled;
    Text      _.cleanSegmentName = segmentName;
    Anything  _.origOutput = origOutput;
    Date      _.firstDate = firstDate;
    Date      _.lastDate = lastDate
  ]];
  Real aux::reset(?);
  Real aux::setVectorizedPrior(k,a,b,lower,upper);
  Real aux::setIndexes(?);
  aux
};

